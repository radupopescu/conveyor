# Conveyor

Conveyor is a set of additional tools and services providing a higher-level, job-based, interface for publishing into CernVM-FS repositories.

# License and copyright

See LICENSE in the project root.
